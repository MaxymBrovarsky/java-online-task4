package com.brom.epam.week4task.logical.game.units.rooms;

import com.brom.epam.week4task.logical.game.units.creatures.Artifact;
import com.brom.epam.week4task.logical.game.units.creatures.Monster;
import com.brom.epam.week4task.logical.game.units.creatures.RoomNPC;

import java.util.ArrayList;
import java.util.List;

public class StandardRoom implements Room {
    private int roomNumber;
    private List<RoomNPC> roomNPCs;
    private int roomStrength;

    public StandardRoom(int roomNumber) {
        this();
        this.roomNumber = roomNumber;
    }
    public StandardRoom() {
       this.roomNPCs = new ArrayList();
    }

    public void addNPC(RoomNPC npc) {
        this.roomNPCs.add(npc);
        if (npc.isEnemy()) {
            this.roomStrength += ((Monster)npc).getStrength();
        } else {
            this.roomStrength -= ((Artifact)npc).getStrengthChange();
        }
    }

    @Override
    public int getRoomNumber() {
        return roomNumber;
    }

    @Override
    public List<RoomNPC> getRoomNPCs() {
        return roomNPCs;
    }

    @Override
    public String toString() {
        return "\n Room №" + this.roomNumber + "\nRoom NPC: \n" + this.roomNPCs.toString();
    }

    @Override
    public boolean isEmpty() {
        return this.roomNPCs.isEmpty();
    }

    @Override
    public int getRoomStrength() {
        return this.roomStrength;
    }

    @Override
    public boolean isDeathRoom(int heroStrength) {
        return this.roomStrength > heroStrength;
    }
}

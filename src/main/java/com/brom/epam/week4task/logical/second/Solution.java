package com.brom.epam.week4task.logical.second;

import com.brom.epam.week4task.logical.arrayutils.ArrayUtils;

import java.util.Arrays;

public class Solution {
    public static final int ARRAY_SIZE = 100;
    public static final int RANDOM_BOUND = 50;
    private int[] array;
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.setArray(ArrayUtils.generateArrayWithRandomValues(ARRAY_SIZE, RANDOM_BOUND));
        Arrays.sort(solution.getArray());
        System.out.println(Arrays.toString(solution.getArray()));
        int[] uniqueElements = ArrayUtils.removeElementsThoseOccurMoreThanOnce(solution.getArray());
        System.out.println(Arrays.toString(uniqueElements));
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
}

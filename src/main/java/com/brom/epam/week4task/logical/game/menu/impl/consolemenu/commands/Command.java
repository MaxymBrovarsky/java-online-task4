package com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands;

public interface Command {
    void execute();
}

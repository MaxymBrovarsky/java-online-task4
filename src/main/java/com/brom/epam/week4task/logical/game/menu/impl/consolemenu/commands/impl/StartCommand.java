package com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.impl;

import com.brom.epam.week4task.logical.game.Application;
import com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.Command;

public class StartCommand implements Command {
    public static final String TITLE = "Start new game";
    private Application application;

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public void execute() {
        System.out.println("Start command");
        application.startNewGame();

    }

    @Override
    public String toString() {
        return TITLE;
    }
}

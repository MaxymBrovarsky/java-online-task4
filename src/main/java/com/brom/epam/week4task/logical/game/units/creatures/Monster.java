package com.brom.epam.week4task.logical.game.units.creatures;

public class Monster extends Creature implements RoomNPC {
    private boolean enemy;
    public Monster(int strength) {
        this.strength = strength;
        this.enemy = true;
    }
    @Override
    public boolean isEnemy() {
        return this.enemy;
    }

    @Override
    public String toString() {
        return "Monster{ strength: " + this.strength + "}";
    }
}

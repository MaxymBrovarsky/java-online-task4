package com.brom.epam.week4task.logical.game.units.factories;

import com.brom.epam.week4task.logical.game.units.creatures.Monster;

import java.util.Random;

public class MonsterFactory {
    private static Random random = new Random();
    private int minStrengthValue;
    private int maxStrengthValue;
    public Monster createMonster() {
        int strength = generateMonsterStrength();
        Monster monster = new Monster(strength);
        return monster;
    }

    private int generateMonsterStrength() {
        int strength = random.nextInt(maxStrengthValue - minStrengthValue) + minStrengthValue;
        return strength;
    }

    public int getMinStrengthValue() {
        return minStrengthValue;
    }

    public int getMaxStrengthValue() {
        return maxStrengthValue;
    }

    public void setMinStrengthValue(int minStrengthValue) {
        this.minStrengthValue = minStrengthValue;
    }

    public void setMaxStrengthValue(int maxStrengthValue) {
        this.maxStrengthValue = maxStrengthValue;
    }
}

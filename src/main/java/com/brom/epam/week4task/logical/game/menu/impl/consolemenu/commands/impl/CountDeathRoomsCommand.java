package com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.impl;

import com.brom.epam.week4task.logical.game.Application;
import com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.Command;

public class CountDeathRoomsCommand implements Command {
    public static final String TITLE = "Count death rooms";
    private Application application;
    @Override
    public void execute() {
        this.application.showDeathRoomsCount();
    }

    @Override
    public String toString() {
        return TITLE;
    }

    public void setApplication(Application application) {
        this.application = application;
    }
}

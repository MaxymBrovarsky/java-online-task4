package com.brom.epam.week4task.logical.game.units.creatures;

public interface RoomNPC {
    boolean isEnemy();
}

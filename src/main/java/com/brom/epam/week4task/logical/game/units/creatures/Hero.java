package com.brom.epam.week4task.logical.game.units.creatures;

import java.util.ArrayList;
import java.util.List;

public class Hero extends Creature {
    private List<Artifact> inventory;
    private boolean alive;
    private int monstersKilled;

    public Hero() {
        this.inventory = new ArrayList<>();
    }

    public void interactWithNPC(RoomNPC npc) {
        System.out.println(this);
        if (npc.isEnemy()) {
            this.fightEnemy(npc);
        } else {
            this.collectArtifact(npc);
        }
        System.out.println(this);
    }
    public void fightEnemy(RoomNPC npc) {
        Monster monster = (Monster) npc;
        if (this.getStrength() < monster.getStrength()) {
            this.alive = false;
        } else {
            ++this.monstersKilled;
        }
    }

    public void collectArtifact(RoomNPC npc) {
        Artifact artifact = (Artifact) npc;
        inventory.add(artifact);
        this.changeStrength(artifact.getStrengthChange());
    }

    public boolean isAlive() {
        return this.alive;
    }

    public int getMonstersKilled() {
        return monstersKilled;
    }

    public void setMonstersKilled(int monstersKilled) {
        this.monstersKilled = monstersKilled;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public String toString() {
        return "Hero{hp: " + this.strength + "; inventory: " + this.inventory + "}\n";
    }
}

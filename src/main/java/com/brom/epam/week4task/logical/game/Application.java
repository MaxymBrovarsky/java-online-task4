package com.brom.epam.week4task.logical.game;

import com.brom.epam.week4task.logical.game.exceptions.GameEndedException;
import com.brom.epam.week4task.logical.game.menu.Menu;
import com.brom.epam.week4task.logical.game.units.rooms.Room;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Application {
    private Scanner scanner;
    private ApplicationContext context;
    private Menu menu;
    private Game game;

    public static void main(String[] args) {
        ApplicationContext context = new GenericXmlApplicationContext("game_config.xml");
        Application application = context.getBean("application", Application.class);
        application.setContext(context);
        application.readInput();
    }

    public Application() {
        this.scanner = new Scanner(System.in);
    }

    public void readInput() {
        menu.showMenu();
        while (true) {
            try {
                int commandNumber = scanner.nextInt();
                try {
                    menu.execute(commandNumber);
                } catch(GameEndedException e) {
                    System.out.println(e.getMessage());
                    this.setMenu(context.getBean("startMenu", Menu.class));
                }
                scanner.nextLine();
                menu.showMenu();
            } catch (InputMismatchException e) {
                System.out.println("Please enter NUMBER of command");
                scanner.nextLine();
            }
        }
    }

    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void startNewGame() {
        this.game = context.getBean("game", Game.class);
        this.setMenu(context.getBean("inGameMenu", Menu.class));
    }

    public Scanner getScanner() {
        return scanner;
    }

    public Game getGame() {
        return game;
    }

    public void showClosedRooms() {
        System.out.println("Closed rooms:");
        List<Room> closedRooms = this.game.getClosedRooms();
        closedRooms.forEach(room -> {
            System.out.println("Room №" + room.getRoomNumber());
        });
    }

    public void showDeathRoomsCount() {
        System.out.println(this.game.countDeathRooms(0));
    }

    public void showRoomOpeningOrderForSurviving() {
        List<Room> roomOrder = this.game.getRoomOpeningOrderForSurviving();
        if (roomOrder.isEmpty()) {
            System.out.println("No possible way");
        } else {
            System.out.println("Room opening order for surviving");
            roomOrder.forEach(room -> {
                System.out.println("Room #" +room.getRoomNumber());
            });
        }

    }

    public void showGameInfo() {
        System.out.println(this.game);
    }
}

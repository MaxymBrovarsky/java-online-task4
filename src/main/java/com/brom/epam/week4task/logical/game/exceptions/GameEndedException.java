package com.brom.epam.week4task.logical.game.exceptions;

public class GameEndedException extends RuntimeException {
    private String message;

    public GameEndedException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}


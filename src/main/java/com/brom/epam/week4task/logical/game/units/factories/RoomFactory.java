package com.brom.epam.week4task.logical.game.units.factories;

import com.brom.epam.week4task.logical.game.units.creatures.Artifact;
import com.brom.epam.week4task.logical.game.units.creatures.Monster;
import com.brom.epam.week4task.logical.game.units.rooms.Room;
import com.brom.epam.week4task.logical.game.units.rooms.StandardRoom;

import java.util.Random;

public class RoomFactory {
    public static final int MONSTER = 0;
    public static final int ARTIFACT = 1;
    public static final int CREATURE_TYPE_NUMBER = 2;
    private int roomNumber;
    private static Random random = new Random();
    private ArtifactFactory artifactFactory;
    private MonsterFactory monsterFactory;

    public void resetRoomNumber() {
        this.roomNumber = 0;
    }
    public Room createStandartRoom(int creaturesInRoom) {
        StandardRoom room = new StandardRoom(roomNumber);
        for (int i = 0; i < creaturesInRoom; i++) {
            int creature = random.nextInt(CREATURE_TYPE_NUMBER);
            if (creature == MONSTER) {
                Monster monster = monsterFactory.createMonster();
                room.addNPC(monster);
            } else if (creature == ARTIFACT) {
                Artifact artifact = artifactFactory.createArtifact();
                room.addNPC(artifact);
            }
        }
        roomNumber++;
//        System.out.println(room);
        return room;
    }

    public void setArtifactFactory(ArtifactFactory artifactFactory) {
        this.artifactFactory = artifactFactory;
    }

    public void setMonsterFactory(MonsterFactory monsterFactory) {
        this.monsterFactory = monsterFactory;
    }
}

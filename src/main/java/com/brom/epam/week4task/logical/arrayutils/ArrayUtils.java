package com.brom.epam.week4task.logical.arrayutils;

import java.util.Random;

public class ArrayUtils {
    public static int[] getIntersection(int[] firstArray, int[] secondArray) {
        int[] intersection = new int[firstArray.length + secondArray.length];
        boolean[] secondArrayAddedFlags = new boolean[secondArray.length];
        int k = 0;
        for (int i = 0; i < firstArray.length; i++) {
            for (int j = 0; j < secondArray.length; j++) {
                if (firstArray[i] == secondArray[j] && !secondArrayAddedFlags[j]) {
                    intersection[k] = firstArray[i];
                    k++;
                    secondArrayAddedFlags[j] = true;
                    break;
                }
            }
        }
        int[] intersectionResult = new int[k];
        for (int i = 0; i < intersectionResult.length; i++) {
            intersectionResult[i] = intersection[i];
        }
        return intersectionResult;
    }
    public static int[] getSymmetricDifference(int[] firstArray, int[] secondArray) {
        int[] intersection = ArrayUtils.getIntersection(firstArray, secondArray);
        int[] firstArrayMinusIntersection = ArrayUtils.getDifference(firstArray, intersection);
        int[] secondArrayMinusIntersection = ArrayUtils.getDifference(secondArray, intersection);
        int[] symmetricDifference = new int[firstArrayMinusIntersection.length + secondArrayMinusIntersection.length];
        for (int i = 0; i < firstArrayMinusIntersection.length; i++) {
            symmetricDifference[i] = firstArrayMinusIntersection[i];
        }
        for (int i = 0; i < secondArrayMinusIntersection.length; i++) {
            symmetricDifference[i + firstArrayMinusIntersection.length] = secondArrayMinusIntersection[i];
        }
        return symmetricDifference;
    }

    public static int[] getDifference(int[] minuend, int[] subtrahend) {
        boolean[] minuendFlags = new boolean[minuend.length];
        boolean[] subtrahendFlags = new boolean[subtrahend.length];
        int[] difference = new int[minuend.length - subtrahend.length];
        int k = 0;
        for (int i = 0; i < minuend.length; i++) {
            for (int j = 0; j < subtrahend.length; j++) {
                if ((minuend[i] == subtrahend[j] && !minuendFlags[i]) && !subtrahendFlags[j]) {
                    minuendFlags[i] = true;
                    subtrahendFlags[j] = true;
                    break;
                }
            }
        }
        for (int i = 0; i < minuendFlags.length; i++) {
            if (!minuendFlags[i]) {
                difference[k++] = minuend[i];
            }
        }
        return difference;
    }

    public static int[] removeElementsThoseOccurMoreThanOnce(int[] array) {
        boolean[] elementsForDeletion = new boolean[array.length];
        int countElementsForDeletion = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    if (!elementsForDeletion[i]) {
                        elementsForDeletion[i] = true;
                        countElementsForDeletion++;
                    }
                    if (!elementsForDeletion[j]) {
                        elementsForDeletion[j] = true;
                        countElementsForDeletion++;
                    }

                }
            }
        }
        int[] uniqueElementsArray = new int[array.length - countElementsForDeletion];
        for (int i = 0, k = 0; i < elementsForDeletion.length; i++) {
            if (!elementsForDeletion[i]) {
                uniqueElementsArray[k++] = array[i];
            }
        }
        return uniqueElementsArray;

    }

    public static int[] generateArrayWithRandomValues(int size, int bound) {
        Random random = new Random();
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(bound);
        }
        return array;
    }

    public static int[] removeDuplicatingElementsInRow(int[] array) {
        boolean[] elementsForDeletion = new boolean[array.length];
        int countElementsForDeletion = 0;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1]) {
                elementsForDeletion[i] = true;
                countElementsForDeletion++;
            }
        }
        int[] arrayWithoutDuplicates = new int[array.length - countElementsForDeletion];
        for (int i = 0, k = 0; i < elementsForDeletion.length; i++) {
            if (!elementsForDeletion[i]) {
                arrayWithoutDuplicates[k++] = array[i];
            }
        }
        return arrayWithoutDuplicates;
    }
}

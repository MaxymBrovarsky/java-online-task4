package com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.impl;

import com.brom.epam.week4task.logical.game.Application;
import com.brom.epam.week4task.logical.game.Game;
import com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.Command;

import java.util.Scanner;

public class OpenRoomCommand implements Command {
    public static final String TITLE = "Open room";
    public static final String NUMBER_REQUEST_NUMBER = "Please enter number of room";
    private Application application;
    @Override
    public void execute() {
        application.showClosedRooms();
        Scanner scanner = application.getScanner();
        System.out.println(NUMBER_REQUEST_NUMBER);
        if (scanner.hasNextInt()) {
            int roomNumber = scanner.nextInt();
            Game game = application.getGame();
            game.interactWithRoom(roomNumber);
        }
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

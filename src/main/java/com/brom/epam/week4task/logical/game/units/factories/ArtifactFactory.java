package com.brom.epam.week4task.logical.game.units.factories;

import com.brom.epam.week4task.logical.game.units.creatures.Artifact;

import java.util.Random;

public class ArtifactFactory {
    private static Random random = new Random();
    private int minBuffValue;
    private int maxBuffValue;
    public Artifact createArtifact() {
        int buff = generateArtifactBuff();
        Artifact artifact = new Artifact(buff);
        return artifact;
    }

    private int generateArtifactBuff() {
        int buff = random.nextInt(maxBuffValue - minBuffValue) + minBuffValue;
        return buff;
    }

    public void setMinBuffValue(int minBuffValue) {
        this.minBuffValue = minBuffValue;
    }

    public void setMaxBuffValue(int maxBuffValue) {
        this.maxBuffValue = maxBuffValue;
    }
}

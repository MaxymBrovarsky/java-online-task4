package com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.impl;

import com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.Command;

public class QuitCommand implements Command {
    public static final String TITLE = "Quit";
    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

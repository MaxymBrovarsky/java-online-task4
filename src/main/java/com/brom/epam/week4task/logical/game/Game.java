package com.brom.epam.week4task.logical.game;

import com.brom.epam.week4task.logical.game.exceptions.GameEndedException;
import com.brom.epam.week4task.logical.game.units.creatures.Hero;
import com.brom.epam.week4task.logical.game.units.creatures.RoomNPC;
import com.brom.epam.week4task.logical.game.units.factories.RoomFactory;
import com.brom.epam.week4task.logical.game.units.rooms.Room;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Game {
    private RoomFactory roomFactory;
    private List<Room> closedRooms;
    private Hero hero;
    private String greetingsMessage;
    private String winMessage;
    private String loseMessage;

    public void interactWithRoom(int number) {
        Room room = closedRooms.get(number);
        List<RoomNPC> npcs = room.getRoomNPCs();
        npcs.forEach(npc -> {
            this.hero.interactWithNPC(npc);
        });
        npcs.clear();
        if (!hero.isAlive()) {
            throw new GameEndedException(loseMessage);
        }
        closedRooms.remove(number);
        if (this.isAllDoorsOpened()) {
            throw new GameEndedException(winMessage);
        }
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public boolean isAllDoorsOpened() {
        return this.closedRooms.isEmpty();
    }

    public void setRoomFactory(RoomFactory roomFactory) {
        this.roomFactory = roomFactory;
    }

    public void init() {
        this.closedRooms = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            this.closedRooms.add(roomFactory.createStandartRoom(1));
        }
        this.printGreetings();
        this.roomFactory.resetRoomNumber();
    }
    private void printGreetings() {
        System.out.println(this.greetingsMessage);
    }

    public void setGreetingsMessage(String greetingsMessage) {
        this.greetingsMessage = greetingsMessage;
    }

    public List<Room> getClosedRooms() {
        return this.closedRooms;
    }

    public void setWinMessage(String winMessage) {
        this.winMessage = winMessage;
    }

    public void setLoseMessage(String loseMessage) {
        this.loseMessage = loseMessage;
    }

    public int countDeathRooms(int roomNumber) {
        if (roomNumber >= this.closedRooms.size()) {
            return 0;
        }
        int deathRoomsCount = 0;
        if (this.getClosedRooms().get(roomNumber).isDeathRoom(this.hero.getStrength())) {
            deathRoomsCount++;
        }
        return deathRoomsCount + countDeathRooms(roomNumber + 1);
    }

    public List<Room> getRoomOpeningOrderForSurviving() {
        List<Room> roomOpeningOrderForSurviving = new ArrayList<>();
        int heroStrengthAfterCollectingArtifacts = hero.getStrength();
        for (Room closedRoom: closedRooms) {
            if (closedRoom.getRoomStrength() < 0) {
                roomOpeningOrderForSurviving.add(closedRoom);
                heroStrengthAfterCollectingArtifacts += Math.abs(closedRoom.getRoomStrength());
            }
        }
        boolean isAnyDeathRoom = false;
        for (Room closedRoom : closedRooms) {
            if (closedRoom.isDeathRoom(heroStrengthAfterCollectingArtifacts)) {
                isAnyDeathRoom = true;
            }
        }
        if (isAnyDeathRoom) {
            return Collections.emptyList();
        }
        List<Room> monsterRooms = new ArrayList<>(closedRooms);
        monsterRooms.removeAll(roomOpeningOrderForSurviving);
        roomOpeningOrderForSurviving.addAll(monsterRooms);
        return roomOpeningOrderForSurviving;
    }

    @Override
    public String toString() {
        return "Hero: " + this.hero + "\n RoomsInfo: \n" + this.closedRooms;
    }
}

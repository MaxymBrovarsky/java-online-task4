package com.brom.epam.week4task.logical.first;

import com.brom.epam.week4task.logical.arrayutils.ArrayUtils;
import java.util.Arrays;
import java.util.Random;

public class Solution {
    public static final int ARRAY_SIZE = 10;
    public static final int RANDOM_BOUND = 10;
    private int[] firstArray;
    private int[] secondArray;

    public static void main(String[] args) {
        Solution s = new Solution();
        System.out.println("first array: " + Arrays.toString(s.getFirstArray()));
        System.out.println("second array: " + Arrays.toString(s.getSecondArray()));
        int[] intersection = ArrayUtils.getIntersection(s.getFirstArray(), s.getSecondArray());
        System.out.println("intersection: " + Arrays.toString(intersection));
        int[] symmetricDifference = ArrayUtils.getSymmetricDifference(s.getFirstArray(), s.getSecondArray());
        System.out.println("symmetric difference: " + Arrays.toString(symmetricDifference));
    }
    public Solution() {
        this.initializeArrays();
        this.fillArraysWithValues();
    }

    private void initializeArrays() {
        this.firstArray = new int[ARRAY_SIZE];
        this.secondArray = new int[ARRAY_SIZE];
    }

    private void fillArraysWithValues() {
        Random r = new Random();
        for (int i = 0; i < ARRAY_SIZE; i++) {
            this.firstArray[i] = r.nextInt(RANDOM_BOUND);
            this.secondArray[i] = r.nextInt(RANDOM_BOUND);
        }
    }

    public int[] getFirstArray() {
        return firstArray;
    }

    public int[] getSecondArray() {
        return secondArray;
    }

}

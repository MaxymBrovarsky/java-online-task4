package com.brom.epam.week4task.logical.game.units.rooms;

import com.brom.epam.week4task.logical.game.units.creatures.RoomNPC;

import java.util.List;

public interface Room {
    List<RoomNPC> getRoomNPCs();
    boolean isEmpty();
    int getRoomNumber();
    int getRoomStrength();
    boolean isDeathRoom(int heroStrength);
}

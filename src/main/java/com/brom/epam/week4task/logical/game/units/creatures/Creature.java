package com.brom.epam.week4task.logical.game.units.creatures;

public class Creature {
    protected int strength;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void changeStrength(int strengthChange) {
        this.strength += strengthChange;
    }
}

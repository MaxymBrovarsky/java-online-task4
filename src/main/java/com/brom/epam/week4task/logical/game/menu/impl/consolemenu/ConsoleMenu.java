package com.brom.epam.week4task.logical.game.menu.impl.consolemenu;

import com.brom.epam.week4task.logical.game.menu.Menu;
import com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.Command;

import java.util.List;

public class ConsoleMenu implements Menu {
    private int indexOffsetFromOne = 1;
    private List<Command> commands;

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void showMenu() {
        System.out.println("Enter number of command");
        for (int i = 0; i < commands.size(); i++) {
            System.out.printf("%d. %s;\n", i + indexOffsetFromOne, commands.get(i).toString());
        }
    }

    @Override
    public void execute(int commandNumber) {
        if (commandNumber < indexOffsetFromOne || commands.size() <= commandNumber - indexOffsetFromOne) {
            System.out.println("Wrong number");
            return;
        }
        commands.get(commandNumber - indexOffsetFromOne).execute();
    }

}

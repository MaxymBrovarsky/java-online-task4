package com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.impl;

import com.brom.epam.week4task.logical.game.Application;
import com.brom.epam.week4task.logical.game.menu.impl.consolemenu.commands.Command;

public class ShowRoomOpeningOrderCommand implements Command {
    public static final String TITLE = "Show room opening order";
    private Application application;

    @Override
    public void execute() {
        this.application.showRoomOpeningOrderForSurviving();
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

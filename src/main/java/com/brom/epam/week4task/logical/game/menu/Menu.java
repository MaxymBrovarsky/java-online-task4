package com.brom.epam.week4task.logical.game.menu;

public interface Menu {
    void showMenu();
    void execute(int commandNumber);
}

package com.brom.epam.week4task.logical.game.units.creatures;

public class Artifact implements RoomNPC {
    private int buff;

    public Artifact(int buff) {
        this.buff = buff;
    }
    @Override
    public boolean isEnemy() {
        return false;
    }

    public int getStrengthChange() {
        return buff;
    }

    public void setBuff(int buff) {
        this.buff = buff;
    }

    @Override
    public String toString() {
        return "Artifact{buff: " + this.buff + "}";
    }
}

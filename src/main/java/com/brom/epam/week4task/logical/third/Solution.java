package com.brom.epam.week4task.logical.third;

import com.brom.epam.week4task.logical.arrayutils.ArrayUtils;

import java.util.Arrays;

public class Solution {
    public static final int ARRAY_SIZE = 30;
    public static final int RANDOM_BOUND = 10;
    private int[] array;
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.setArray(ArrayUtils.generateArrayWithRandomValues(ARRAY_SIZE, RANDOM_BOUND));
        int[] arrayWithoutDuplicates = ArrayUtils.removeDuplicatingElementsInRow(solution.getArray());
        System.out.println(Arrays.toString(arrayWithoutDuplicates));
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
}

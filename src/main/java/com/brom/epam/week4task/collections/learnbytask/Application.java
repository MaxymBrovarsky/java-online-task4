package com.brom.epam.week4task.collections.learnbytask;

import com.brom.epam.week4task.collections.learnbytask.bouquet.Bouquet;
import com.brom.epam.week4task.collections.learnbytask.menu.Menu;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {
    private Scanner scanner = new Scanner(System.in);
    private Menu menu;
    private ApplicationContext context;
    private Bouquet currentBouquet;

    public static void main(String[] args) {
        ApplicationContext context = new GenericXmlApplicationContext("flowers_config.xml");
        Application application = context.getBean("application", Application.class);
        application.setContext(context);
        application.readInput();
    }

    public void readInput() {
        menu.showMenu();
        while (true) {
            try {
                int commandNumber = scanner.nextInt();
                menu.execute(commandNumber);
                scanner.nextLine();
                menu.showMenu();
            } catch (InputMismatchException e) {
                System.out.println("Please enter NUMBER of command");
                scanner.nextLine();
            }
        }
    }

    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void startBouquetConstruction(){
        this.setMenu(this.context.getBean("bouquetConstructionMenu",Menu.class));
        this.currentBouquet = new Bouquet();
    }

    public void exitFromConstructionMode() {
        this.setMenu(this.context.getBean("startMenu",Menu.class));
    }

    public Bouquet getCurrentBouquet() {
        return currentBouquet;
    }
}

package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.Application;

public class ExitFromConstructionCommand implements Command {
    public static final String TITLE = "Exit from construction mode";
    private Application application;

    @Override
    public void execute() {
        this.application.exitFromConstructionMode();
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

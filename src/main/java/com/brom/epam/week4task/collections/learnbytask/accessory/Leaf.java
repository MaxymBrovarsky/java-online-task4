package com.brom.epam.week4task.collections.learnbytask.accessory;

public class Leaf implements Accessory {
    private int price;

    public Leaf(int price) {
        this.price = price;
    }

    @Override
    public int getPrice() {
        return this.price;
    }
}

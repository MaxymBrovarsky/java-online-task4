package com.brom.epam.week4task.collections.learnbytask.bouquet;

public interface BouquetComponent {
    int getPrice();
}

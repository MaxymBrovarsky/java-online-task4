package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.Application;

public class StartBouquetConstructionCommand implements Command{
    public static final String TITLE = "Start bouquet construction";
    private Application application;
    @Override
    public void execute() {
        this.application.startBouquetConstruction();
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

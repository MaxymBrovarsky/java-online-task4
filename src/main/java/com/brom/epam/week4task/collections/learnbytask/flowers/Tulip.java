package com.brom.epam.week4task.collections.learnbytask.flowers;

public class Tulip extends Flower {

    public Tulip(int stemLength, int freshnessInPercents) {
        super(stemLength, freshnessInPercents);
    }

    @Override
    public String toString() {
        String superValue = super.toString();
        return "Tulip {" + superValue + "}";
    }
}

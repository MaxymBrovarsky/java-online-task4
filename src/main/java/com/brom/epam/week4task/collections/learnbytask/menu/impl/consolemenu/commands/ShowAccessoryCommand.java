package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.repository.AccessoryRepository;

public class ShowAccessoryCommand implements Command {
    public static final String TITLE = "Show accessories list";
    private AccessoryRepository accessoryRepository;
    @Override
    public void execute() {
        System.out.println("Accessory list:");
        this.accessoryRepository.getAccessories().forEach(accessory -> {
            System.out.println(accessory);
        });
    }

    public void setAccessoryRepository(AccessoryRepository accessoryRepository) {
        this.accessoryRepository = accessoryRepository;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

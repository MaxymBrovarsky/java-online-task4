package com.brom.epam.week4task.collections.stringarraylist;

import org.apache.commons.lang3.ArrayUtils;
import java.util.Arrays;

public class StringArrayList {
    private static final int initialSize = 5;
    private String[] elements;
    private int size;

    public StringArrayList() {
        this.elements = new String[initialSize];
        this.size = 0;
    }

    public void add(String element) {
        this.elements[size] = element;
        this.size++;
        if (this.size == elements.length) {
            this.elements = Arrays.copyOf(this.elements, this.size * 2);
        }
    }

    public void remove(int index) {
        ArrayUtils.remove(this.elements, index);
        this.size--;
    }

    public String get(int index) {
        return this.elements[index];
    }

    public int getSize() {
        return this.size;
    }
}

package com.brom.epam.week4task.collections.learnbytask.repository.impl;

import com.brom.epam.week4task.collections.learnbytask.flowers.Flower;
import com.brom.epam.week4task.collections.learnbytask.flowers.Rose;
import com.brom.epam.week4task.collections.learnbytask.flowers.Tulip;
import com.brom.epam.week4task.collections.learnbytask.repository.FlowersRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryFlowersRepository implements FlowersRepository {
    private List<Flower> flowers;

    public InMemoryFlowersRepository() {
        this.flowers = new ArrayList<>();
        this.flowers.add(new Rose(70,100,"blue"));
        this.flowers.add(new Rose(60,90,"red"));
        this.flowers.add(new Rose(50,80,"purple"));
        this.flowers.add(new Tulip(50,50));
        this.flowers.add(new Tulip(40,95));
        this.flowers.add(new Tulip(45,100));

    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public Flower getFlowerById(int id) {
        return this.flowers.get(id);
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }
}

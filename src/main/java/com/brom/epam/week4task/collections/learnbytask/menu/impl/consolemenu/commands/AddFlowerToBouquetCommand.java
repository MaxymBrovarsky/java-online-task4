package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.Application;
import com.brom.epam.week4task.collections.learnbytask.bouquet.Bouquet;
import com.brom.epam.week4task.collections.learnbytask.repository.FlowersRepository;

import java.util.Scanner;

public class AddFlowerToBouquetCommand implements Command {
    public static final String TITLE = "Add flower to bouquet";
    private Application application;
    private FlowersRepository flowersRepository;
    @Override
    public void execute() {
        Scanner scanner = application.getScanner();
        Bouquet bouquet = application.getCurrentBouquet();
        System.out.println("enter index of flower");
        if (scanner.hasNextInt()) {
            int flowerIndex = scanner.nextInt();
            bouquet.addFlower(flowersRepository.getFlowerById(flowerIndex));
        }
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public void setFlowersRepository(FlowersRepository flowersRepository) {
        this.flowersRepository = flowersRepository;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

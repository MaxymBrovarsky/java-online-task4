package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.Application;
import com.brom.epam.week4task.collections.learnbytask.bouquet.Bouquet;
import com.brom.epam.week4task.collections.learnbytask.flowers.Flower;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class FindFlowerByStemLengthCommand implements Command {
    public static final String TITLE = "Find flower by stem length";
    private Application application;
    @Override
    public void execute() {
        Scanner scanner = application.getScanner();
        try {
            System.out.println("enter low bound of stem length");
            int lowBound = scanner.nextInt();
            System.out.println("enter high bound of stem length");
            int highBound = scanner.nextInt();
            Bouquet bouquet = application.getCurrentBouquet();
            List<Flower> flowers = bouquet.findFlowersByStemLengthInRange(lowBound, highBound);
            System.out.println(flowers);
        } catch (InputMismatchException e) {
            System.out.println("enter only integers");
        }
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

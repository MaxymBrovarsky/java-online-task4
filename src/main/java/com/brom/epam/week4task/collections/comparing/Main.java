package com.brom.epam.week4task.collections.comparing;



import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();
        ArrayList<CountryCapitalPair> countryCapitalPairs = main.initialize();
        System.out.println(countryCapitalPairs);
        Collections.sort(countryCapitalPairs);
        System.out.println(countryCapitalPairs);
        Comparator<CountryCapitalPair> capitalComparator = main.createCapitalComparator();
        Collections.sort(countryCapitalPairs, capitalComparator);
        System.out.println(countryCapitalPairs);
        CountryCapitalPair ukraine = new CountryCapitalPair("Ukraine", "Kyiv");
        int indexOfUkraine = Collections.binarySearch(countryCapitalPairs, ukraine, capitalComparator);
        System.out.println(indexOfUkraine);
    }

    public  ArrayList<CountryCapitalPair> initialize() {
        ArrayList<CountryCapitalPair> countryCapitalPairs = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("/country_capital.txt")))){
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                String[] countryCapital = line.split("-");
                countryCapitalPairs.add(new CountryCapitalPair(countryCapital[0], countryCapital[1]));
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return countryCapitalPairs;
    }

    public Comparator<CountryCapitalPair> createCapitalComparator() {
        Comparator<CountryCapitalPair> comparator = Comparator.comparing(CountryCapitalPair::getCapital);
        return comparator;
    }
}

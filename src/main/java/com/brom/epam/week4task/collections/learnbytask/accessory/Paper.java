package com.brom.epam.week4task.collections.learnbytask.accessory;

public class Paper implements Accessory {
    private int price;

    public Paper(int price) {
        this.price = price;
    }

    @Override
    public int getPrice() {
        return this.price;
    }
}

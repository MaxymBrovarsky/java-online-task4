package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu;

import com.brom.epam.week4task.collections.learnbytask.menu.Menu;
import com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands.Command;

import java.util.List;

public class ConsoleMenu implements Menu {
    private List<Command> commands;
    @Override
    public void showMenu() {
        for (int i = 0; i < commands.size(); i++) {
            System.out.printf("%d. %s\n", i, commands.get(i));
        }
    }

    @Override
    public void execute(int index) {
        if (index < 0 || index >= commands.size()) {
            System.out.println("Enter correct number");
            return;
        }
        this.commands.get(index).execute();
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }
}

package com.brom.epam.week4task.collections.learnbytask.repository;

import com.brom.epam.week4task.collections.learnbytask.accessory.Accessory;

import java.util.List;

public interface AccessoryRepository {
    List<Accessory> getAccessories();
    Accessory getAccessoryById(int id);
}

package com.brom.epam.week4task.collections.learnbytask.menu;

public interface Menu {
    void showMenu();
    void execute(int index);
}

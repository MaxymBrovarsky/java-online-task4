package com.brom.epam.week4task.collections.learnbytask.bouquet;

import com.brom.epam.week4task.collections.learnbytask.accessory.Accessory;
import com.brom.epam.week4task.collections.learnbytask.flowers.Flower;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Bouquet {
    private List<Flower> flowers;
    private List<Accessory> accessories;
    private int price;

    public Bouquet() {
        this.flowers = new ArrayList<>();
        this.accessories = new ArrayList<>();
    }

    public void addFlower(Flower flower) {
        this.flowers.add(flower);
    }

    public void addAccessory(Accessory accessory) {
        this.accessories.add(accessory);
    }

    public int getPrice() {
        this.countPrice();
        return this.price;
    }

    private void countPrice() {
        flowers.forEach(flower -> {
            this.price += flower.getPrice();
        });

        accessories.forEach(accessory -> {
            this.price += accessory.getPrice();
        });
    }

    public void sortFlowersByFreshness() {
        Collections.sort(this.flowers);
    }

    public List<Flower> findFlowersByStemLengthInRange(int startStemLength, int endStemLength) {
        List<Flower> flowers = new ArrayList<>();
        this.flowers.forEach(flower -> {
            int stemLength = flower.getStemLength();
            if (stemLength > startStemLength && stemLength < endStemLength) {
                flowers.add(flower);
            }
        });
        return flowers;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        return "Bouquet(price =" + price +")\nflowers in bouequet:\n" + this.flowers + "\naccessories in bouquet:\n"+
                this.accessories;
    }
}

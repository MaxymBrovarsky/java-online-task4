package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.Application;

import java.util.Collections;

public class SortFlowersInBouquetCommand implements Command {
    public static final String TITLE = "Sort flowers in bouquet";
    private Application application;
    @Override
    public void execute() {
        Collections.sort(this.application.getCurrentBouquet().getFlowers());
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

package com.brom.epam.week4task.collections.learnbytask.repository;

import com.brom.epam.week4task.collections.learnbytask.flowers.Flower;

import java.util.List;

public interface FlowersRepository {
    List<Flower> getFlowers();
    Flower getFlowerById(int id);
}

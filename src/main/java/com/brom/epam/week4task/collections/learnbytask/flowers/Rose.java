package com.brom.epam.week4task.collections.learnbytask.flowers;

public class Rose extends Flower {
    private String color;

    public Rose(int stemLength, int freshnessInPercents) {
        super(stemLength, freshnessInPercents);
    }

    public Rose(int stemLength, int freshnessInPercents, String color) {
        super(stemLength, freshnessInPercents);
        this.color = color;
    }

    @Override
    public String toString() {
        String superValue = super.toString();
        return "Rose {" + superValue + "}";
    }
}

package com.brom.epam.week4task.collections.learnbytask.repository.impl;

import com.brom.epam.week4task.collections.learnbytask.accessory.Accessory;
import com.brom.epam.week4task.collections.learnbytask.accessory.Leaf;
import com.brom.epam.week4task.collections.learnbytask.accessory.Paper;
import com.brom.epam.week4task.collections.learnbytask.repository.AccessoryRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryAccessoryRepository implements AccessoryRepository {
    private List<Accessory> accessories;

    public InMemoryAccessoryRepository() {
        this.accessories = new ArrayList<>();
        this.accessories.add(new Leaf(10));
        this.accessories.add(new Leaf(15));
        this.accessories.add(new Leaf(20));
        this.accessories.add(new Paper(10));
        this.accessories.add(new Paper(25));
        this.accessories.add(new Paper(30));
    }

    @Override
    public List<Accessory> getAccessories() {
        return accessories;
    }

    @Override
    public Accessory getAccessoryById(int id) {
        return this.accessories.get(id);
    }

    public void setAccessories(List<Accessory> accessories) {
        this.accessories = accessories;
    }
}

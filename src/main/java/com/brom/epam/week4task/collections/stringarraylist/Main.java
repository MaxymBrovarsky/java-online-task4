package com.brom.epam.week4task.collections.stringarraylist;

import java.util.ArrayList;
import java.util.Random;

public class Main {
    public static final String[] TEST_DATA = new String[] {"String1", "String2", "String3", "String4", "String5",
            "String6", "String7", "String8", "String9", "String10"};
    public static final int[] INDEXES_FOR_REMOVING = new int[] {7, 5, 3, 1};
    public static void main(String[] args) {
        testCustomArrayList();
        testArrayList();
    }

    private static void testCustomArrayList() {
        long startTime = System.currentTimeMillis();
        StringArrayList arrayList = new StringArrayList();
        for (int i = 0; i < TEST_DATA.length; i++) {
            arrayList.add(TEST_DATA[i]);
        }
        for (int i = 0; i < arrayList.getSize(); i++) {
            System.out.println(arrayList.get(i));
        }
        for (int i = 0; i < INDEXES_FOR_REMOVING.length; i++) {
            arrayList.remove(INDEXES_FOR_REMOVING[i]);
        }
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("My String ArrayList perfomance time: " + elapsedTime);
    }

    private static void testArrayList() {
        long startTime = System.currentTimeMillis();
        ArrayList<String> arrayList = new ArrayList<>(5);
        for (int i = 0; i < TEST_DATA.length; i++) {
            arrayList.add(TEST_DATA[i]);
        }
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(arrayList.get(i));
        }
        for (int i = 0; i < INDEXES_FOR_REMOVING.length; i++) {
            arrayList.remove(INDEXES_FOR_REMOVING[i]);
        }
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Original ArrayList perfomance time: " + elapsedTime);
    }
}

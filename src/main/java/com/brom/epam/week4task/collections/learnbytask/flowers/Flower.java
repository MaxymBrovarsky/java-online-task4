package com.brom.epam.week4task.collections.learnbytask.flowers;

import com.brom.epam.week4task.collections.learnbytask.bouquet.BouquetComponent;

public abstract class Flower implements Comparable<Flower>, BouquetComponent {
    private int stemLength;
    private int freshnessInPercents;
    private int price;

    public Flower(int stemLength, int freshnessInPercents) {
        this.stemLength = stemLength;
        this.freshnessInPercents = freshnessInPercents;
    }

    @Override
    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getStemLength() {
        return stemLength;
    }

    public void setStemLength(int stemLength) {
        this.stemLength = stemLength;
    }

    public int getFreshnessInPercents() {
        return freshnessInPercents;
    }

    public void setFreshnessInPercents(int freshnessInPercents) {
        this.freshnessInPercents = freshnessInPercents;
    }

    @Override
    public int compareTo(Flower o) {
        return this.freshnessInPercents - o.getFreshnessInPercents();
    }

    @Override
    public String toString() {
        return "freshness: " + freshnessInPercents + "%; stemLength: " + stemLength + "";
    }
}

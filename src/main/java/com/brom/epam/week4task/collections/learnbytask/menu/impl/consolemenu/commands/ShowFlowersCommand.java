package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.repository.FlowersRepository;

public class ShowFlowersCommand implements Command{
    public static final String TITLE = "Show flowers list";
    private FlowersRepository flowersRepository;
    @Override
    public void execute() {
        System.out.println("Flowers list:");
        flowersRepository.getFlowers().forEach(flower -> {
            System.out.println(flower);
        });
    }

    public void setFlowersRepository(FlowersRepository flowersRepository) {
        this.flowersRepository = flowersRepository;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.Application;
import com.brom.epam.week4task.collections.learnbytask.bouquet.Bouquet;

public class ShowBouquetCommand implements Command {
    public static final String TITLE = "Show bouquet";
    private Application application;
    @Override
    public void execute() {
        System.out.println(this.application.getCurrentBouquet());
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}

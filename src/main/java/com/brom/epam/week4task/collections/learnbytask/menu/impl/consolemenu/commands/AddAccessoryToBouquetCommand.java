package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

import com.brom.epam.week4task.collections.learnbytask.Application;
import com.brom.epam.week4task.collections.learnbytask.bouquet.Bouquet;
import com.brom.epam.week4task.collections.learnbytask.repository.AccessoryRepository;

import java.util.Scanner;

public class AddAccessoryToBouquetCommand implements Command {
    public static final String TITLE = "Add accessory to bouquet";
    private Application application;
    private AccessoryRepository accessoryRepository;
    @Override
    public void execute() {
        Scanner scanner = application.getScanner();
        Bouquet bouquet = application.getCurrentBouquet();
        System.out.println("enter index of flower");
        if (scanner.hasNextInt()) {
            int flowerIndex = scanner.nextInt();
            bouquet.addAccessory(accessoryRepository.getAccessoryById(flowerIndex));
        }

    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public void setAccessoryRepository(AccessoryRepository accessoryRepository) {
        this.accessoryRepository = accessoryRepository;
    }

    @Override
    public String toString() {
        return TITLE;
    }
}



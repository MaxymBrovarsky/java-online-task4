package com.brom.epam.week4task.collections.learnbytask.accessory;

import com.brom.epam.week4task.collections.learnbytask.bouquet.BouquetComponent;

public interface Accessory extends BouquetComponent {
}

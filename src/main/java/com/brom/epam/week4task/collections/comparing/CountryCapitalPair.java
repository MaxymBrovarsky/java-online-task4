package com.brom.epam.week4task.collections.comparing;

public class CountryCapitalPair implements Comparable {
    private String country;
    private String capital;

    public CountryCapitalPair(String counrty, String capital) {
        this.country = counrty;
        this.capital = capital;
    }

    public String getCountry() {
        return this.country;
    }

    public String getCapital() {
        return this.capital;
    }
    @Override
    public int compareTo(Object o) {
        if (o instanceof CountryCapitalPair) {
            CountryCapitalPair anotherCountryCapitalPair = (CountryCapitalPair) o;
            return this.country.compareTo(anotherCountryCapitalPair.country);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return this.country + " - " + this.capital;
    }
}

package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

public interface Command {
    void execute();
}

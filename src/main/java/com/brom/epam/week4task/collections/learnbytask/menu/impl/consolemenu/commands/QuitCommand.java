package com.brom.epam.week4task.collections.learnbytask.menu.impl.consolemenu.commands;

public class QuitCommand implements Command {
    public static final String TITLE = "Quit";
    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String toString() {
        return TITLE;
    }
}
